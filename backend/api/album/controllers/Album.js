'use strict';
const fse = require('fs-extra');
const path = require('path');

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {

  /**
   * Creating an album: just tack on the user id.
   */
  create(ctx) {
    return strapi.services.album.create(Object.assign(ctx.request.body, {
      user: ctx.state.user.id
    }));
  },


  /**
   * Update order of photos
   */
  updatePhotoOrder: async (ctx) => {
    try {
      // Pull photoOrder and save into album
      let photoOrder = ctx.request.body.photoOrder;
      await strapi.services.album.update(ctx.params, { photoOrder });
      // Send response
      return ctx.send({ok: true});
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },


  /**
   * Delete an album
   */
  delete: async (ctx) => {
    try {
      // Get the album data
      let album = await strapi.models.album
        .where('id', ctx.params.id)
        .fetch({
          withRelated: ['photos']
        })
        .then(album => album.toJSON())

      // Loop through the photos and delete them
      for (let photo of album.photos) {
        // Kill the file's folder
        await fse.remove(
          path.resolve(__dirname, '../../../../uploads', String(photo.id))
        );
        // Remove the record
        await strapi.services.photo.delete({id: photo.id});
      }

      // Now remove the album
      await strapi.services.album.delete(ctx.params);

      return ctx.send({ok: true});
    } catch (err) {
      console.log(err);
      return ctx.badImplementation();
    }
  }


};
