'use strict';
const sharp = require('sharp');

/**
 * Read the documentation () to implement custom service functions
 */

module.exports = {

  /**
   * Resize a photo
   */
  resizePhoto: async (src, dest, size) => {
    await sharp(src)
      .resize(size, size, {fit: "inside"})
      .jpeg()
      .toFile(dest);
  }

};
