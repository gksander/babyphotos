'use strict';
const fs = require('fs');
const fse = require('fs-extra');
const path = require('path');
const { promisify } = require('util');
const imageSize = promisify(require('image-size'));
const uuidv1 = require('uuid/v1');

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {

  /**
   * Create a photo record
   */
  create: async (ctx) => {
    let user = ctx.state.user,
        body = ctx.request.body,
        file = body.files.file,
        fields = body.fields,
        photo = null;
    try {
      // Create a DB record.
      photo = await strapi.services.photo.create({
        user: user.id,
        album: fields.album,
        originalName: file.name,
        uuid: uuidv1()
      });

      // Create a home for the image
      let newHome = path.join(__dirname, '../../../public/uploads', photo.attributes.uuid);
      await fse.mkdir(newHome);
      
      // Make a hero version
      let heroPath = path.join(newHome, "hero.jpg");
      await strapi.services.photo.resizePhoto(
        file.path,
        heroPath,
        2000
      );

      // And a thumbnail
      await strapi.services.photo.resizePhoto(
        file.path,
        heroPath.replace("hero.jpg", "thumbnail.jpg"),
        500
      );

      // Pull hero dimensions and save those
      const heroDimensions = await imageSize(heroPath);

      // Save hero dimensions into photo object
      photo.set('heroWidth', heroDimensions.width);
      photo.set('heroHeight', heroDimensions.height);
      await photo.save();

      // Delete the original
      await fse.remove(file.path);

      return ctx.send(photo);
    } catch(err) {
      console.log(err);
      // Remove file.
      if (file && file.path) {
        await fse.remove(file.path);
      }
      // Remove db record
      if (photo) {
        await photo.remove();
      }

      return ctx.badImplementation();
    }
  },


  /**
   * Delete
   */
  delete: async (ctx) => {
    try {
      let photoId = ctx.params.id;

      // Delete the photo
      await strapi.services.photo.delete({id: photoId});

      // Delete the folder
      await fse.remove(
        path.resolve(__dirname, '../../../public/uploads', photoId)
      );

      // Send response
      return ctx.send({ok: true});
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },



  /**
   * Get a served image
   */
  getImage: async (ctx) => {
    try {
      let jwt = ctx.query.jwt,
          photoId = ctx.params.id,
          version = ctx.params.version || "thumbnail";

      // No jwt in query string, bad request
      if (!jwt) return ctx.unauthorized();

      // Try to verify jwt
      let payload = await strapi.services.jwt.verify(jwt);
      if (!payload) return ctx.unauthorized();
      
      // Stream the image out

      // Read the file, send it down as stream
      let rs = fs.createReadStream(
        path.resolve(__dirname, '../../../../uploads', photoId, `${version}.jpg`)
      );
      ctx.set('Cache-Control', 'public, max-age=604800'); // Cache for 7 days
      ctx.set('Expires', new Date(Date.now() + 604800000).toUTCString()); // Cache for 7 days
      ctx.type = "image/jpeg";
      ctx.body = rs;
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },



  /**
   * Download an image
   */
  downloadImage: async (ctx) => {
    try {
      let jwt = ctx.query.jwt,
          photoId = ctx.params.id,
          photo = await strapi.services.photo.findOne({id: photoId}),
          originalName = photo.attributes.originalName.replace(/\.\w{1,6}$/, ".jpg"),
          uuid = photo.attributes.uuid;

      // No jwt in query string, bad request
      if (!jwt) return ctx.unauthorized();

      // Try to verify jwt
      let payload = await strapi.services.jwt.verify(jwt);
      if (!payload) return ctx.unauthorized();
      
      // Stream the image out

      // Read the file, send it down as stream
      let rs = fs.createReadStream(
        path.resolve(__dirname, '../../../public/uploads', uuid, `hero.jpg`)
      );
      ctx.attachment(originalName);
      ctx.type = "application/octet-stream";
      ctx.body = rs;
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },


};
