'use strict';

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {

  /**
   * Get user
   */
  getUser: async (ctx) => {
    try {
      // Send back just the user.
      let user = ctx.state.user;
      if (!user) return ctx.badRequest();

      // Generate a token
      const jwt = strapi.services.jwt.issue({id: user.id});

      return ctx.send({ user, jwt });
    } catch (err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },



  /**
   * Attempt a guest login
   */
  guestLogin: async (ctx) => {
    try {
      // Make sure we've got a password
      let password = ctx.request.body.password;
      if (!password) return ctx.badRequest(`No password supplied`);

      // Get the guest user
      let guestUser = await strapi.models.user
        .where('email', 'guest+gksander93@gmail.com')
        .fetch({
          withRelated: ['role']
        })
        .then(user => user.toJSON())

      // Make sure we've got a guest user
      if (!guestUser) return ctx.badImplementation(`No guest user!`)

      // Check password
      const validPassword = strapi.services.user.validatePassword(password, guestUser.password);
      if (!validPassword) return ctx.unauthorized(`Wrong password.`);

      // Generate token for user
      const jwt = strapi.services.jwt.issue({id: guestUser.id});
      
      // Send back data
      delete guestUser.password;
      return ctx.send({ user: guestUser, jwt });
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },


  /**
   * Attempt a guest login
   */
  adminLogin: async (ctx) => {
    try {
      // Pull request data
      let body = ctx.request.body,
          email = body.email,
          password = body.password;
      if (!email || !password) return ctx.badRequest(`Email and password required.`);
      
      let user = await strapi.models.user
        .where('email', email)
        .fetch({
          withRelated: ['role']
        })
        .then(user => {
          return user ? user.toJSON() : null
        })
      
      // Make sure we've got a guest user
      if (!user) return ctx.badRequest(`Bad email.`)

      // Check password
      const validPassword = strapi.services.user.validatePassword(password, user.password);
      if (!validPassword) return ctx.unauthorized(`Wrong password.`);

      // Generate token for user
      const jwt = strapi.services.jwt.issue({id: user.id});
      
      // Send back data
      delete user.password;
      return ctx.send({ user, jwt });
    } catch(err) {
      console.log(err);
      return ctx.badImplementation();
    }
  },


};
