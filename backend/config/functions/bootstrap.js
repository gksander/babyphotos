'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 */

module.exports = cb => {
  strapi.models.user = strapi.plugins['users-permissions'].models.user;
  strapi.services.user = strapi.plugins['users-permissions'].services.user;
  strapi.services.jwt = strapi.plugins['users-permissions'].services.jwt;


  cb();
};
