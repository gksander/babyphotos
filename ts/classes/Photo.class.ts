export default class Photo {
  id: number = 0;
  uuid: string = "";
  originalName: string = "";
  heroWidth: number = 0;
  heroHeight: number = 0;
  description: string = "";
  order: number = 0;
  cover: boolean = false;
  
  // Local state
  descriptionDirty: boolean = false;


  constructor(data: $gks.IPhotoData) {
    this.id = (data && data.id) || 0;
    this.uuid = (data && data.uuid) || "";
    this.originalName = (data && data.originalName) || "";
    this.heroWidth = (data && data.heroWidth) || 0;
    this.heroHeight = (data && data.heroHeight) || 0;
    this.order = (data && data.order) || 0;
    this.description = (data && data.description) || "";
    this.cover = (data && data.cover) || false;
  }


  /**
   * Other properties
   */
  url(mode: "hero"|"thumbnail" = "hero") {
    // @ts-ignore
    return this.id ? `${process.env.API_BASE}/uploads/${this.uuid}/${mode}.jpg` : ``;
  }

}