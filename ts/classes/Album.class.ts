import Photo from "./Photo.class";

export default class Album {
  // Data
  id: number = 0;
  title: string = "";
  description: string = "";
  photoOrder: Array<number> = [];
  // Related data
  photos: Array<$gks.IPhotoData> = [];
  $photos: Array<Photo> = [];

  /**
   * Constructor
   */
  constructor(data: $gks.IAlbumData) {
    this.$setData(data);
  }
  // Set data
  $setData(data: $gks.IAlbumData) {
    this.id = (data && data.id) || 0;
    this.title = (data && data.title) || "";
    this.description = (data && data.description) || "";
    this.photoOrder = (data && data.photoOrder) || [];
    // Related data
    if (data && data.photos && data.photos.length > 0) {
      // Transform the photos
      this.$photos = data.photos.map(i => new Photo(i));
      // Set the order on them based on photoOrder array
      this.photoOrder.forEach((id, i) => {
        let photo = this.$photos.find(photo => photo.id == id);
        if (photo) photo.order = i;
      })
    }
  }

  // Sorted photos
  get $sortedPhotos(): Array<Photo> {
    return [...this.$photos].sort((a, b) => a.order - b.order);
  }

  set $sortedPhotos(photos: Array<Photo>) {
    photos.forEach((photo, i) => {
      photo.order = i;
    });
  }

  // Cover image url
  get coverUrl(): string {
    // No photos, show remy
    if (this.$photos.length == 0) return `/img/remy.jpg`;

    // First try to find photo marked as cover
    let coverPhoto = this.$photos.find(photo => photo.cover);
    if (coverPhoto) {
      // @ts-ignore
      return `${process.env.API_BASE}/uploads/${coverPhoto.uuid}/hero.jpg`
    }

    // Otherwise, just use the first photo
    let firstPhoto = this.$sortedPhotos[0];
    // @ts-ignore
    return `${process.env.API_BASE}/uploads/${firstPhoto.uuid}/hero.jpg`
  }


}