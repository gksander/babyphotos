export default class User {
  id: number = 0;
  role: {name: string} = {name: "Public"};

  // Set data
  constructor(data: $gks.IUserData) {
    this.id = (data && data.id) || 0;
    this.role = (data && data.role) || {name: "Public"};
  }


  /**
   * Is user a guest?
   */
  get isGuest(): boolean {
    return (
      this.role &&
      this.role.name != undefined &&
      this.role.name === 'Authenticated'
    )
  }

  /**
   * Is user an admin
   */
  get isAdmin(): boolean {
    return (
      this.role &&
      this.role.name != undefined &&
      this.role.name === 'Admin'
    )
  }

  /**
   * Can view site only if guest or admin
   */
  get canViewSite(): boolean {
    return this.isGuest || this.isAdmin;
  }


}