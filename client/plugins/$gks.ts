import Vue from 'vue';
import User from '../../ts/classes/User.class'
import Album from '../../ts/classes/Album.class'

export default ({ app }: any, inject: Function) => {

  const bareUser = new User(null);

  const data = Vue.observable({
    user: bareUser,
    albums: [],
    API_BASE: process.env.API_BASE,

    loading: {
      value: false,
      message: "Please stand by."
    },

    snackbar: {
      message: "",
      type: "success"
    }
  });

  const actions = {
    async getUser() {
      try {
        // Don't need to pull data if we already have it.
        if (data.user != bareUser) return;
        if (!localStorage.getItem('gksAuthToken')) return;
        
        // Otherwise, fetch the data.
        const { user, jwt } = await app.$axios
          .get(`/misc/getUser`)
          .then((res: any) => res.data);

        // Set the user data
        data.user = new User(user);
        localStorage.setItem('gksAuthToken', jwt);
      } catch(err) {
        data.user = bareUser;
        localStorage.removeItem('gksAuthToken');
      }
    }, // End getUser

    // Log user out
    logout() {
      localStorage.removeItem('gksAuthToken');
      data.user = bareUser;
    }, // End logout

    // Get albums
    async getAlbums() {
      try {
        if (data.albums.length > 0) return;

        let albumsData = await app.$axios.get(`/albums`).then((res: any) => res.data);
        data.albums = albumsData.map(i => new Album(i));
      } catch(err) {
        console.log(err);
      }
    },

    /**
     * Set the snackbar
     */
    snackbar(payload: any) {
      if (payload) {
        data.snackbar = payload;
      } else {
        data.snackbar = {
          message: "",
          type: "success"
        }
      }
    },

    /**
     * Change loading data
     */
    setLoading(payload) {
      let defaultMessage = "Please stand by.";
      if (payload === true) {
        data.loading = {
          value: true,
          message: defaultMessage
        }
      } else if (payload === false) {
        data.loading = {
          value: false,
          message: ""
        }
      }
      else { // Custom
        data.loading = payload;
      }
    }

  }; // End actions


  // Inject into our nuxt app.
  const $gks = { data, actions };
  inject('gks', $gks);
}