export default function ({ $axios, app }) {
  $axios.onRequest((config) => {
    let token = localStorage.getItem('gksAuthToken');
    // console.log(token);
    if (token) {
      config.headers.common['Authorization'] = `Bearer ${token}`;
    }
  })
 }