export default async ({ app, redirect, route }) => {
  try {
    // Check if user is logged in
    if (!app.$gks.data.user.isAdmin) {
      throw "Not logged in."
    }
  } catch(err) {
    return redirect(`/`);
  }
}