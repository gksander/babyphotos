export default async function ({ app, route, query, redirect }) {
  if (query.jwt) {
    localStorage.setItem('gksAuthToken', query.jwt);
    // Get query string and strip the jwt from it
    let q = Object.assign({}, query);
    delete q.jwt;
    // Redirect to remove the jwt
    redirect(route.path, q);
  }
  await app.$gks.actions.getUser();
}