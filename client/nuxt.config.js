import colors from 'vuetify/es5/util/colors';
const path = require('path');

const isProduction = process.env.NODE_ENV == 'production';

const API_BASE = isProduction ? `https://bp-api.gksander.com` : `http://localhost:3600`;

console.log(API_BASE)

const config = {
  mode: 'spa',

  /**
   * Server values
   */
  server: {
    host: '0.0.0.0',
    port: 3601
  },

  /**
   * Env variables
   */
  env: {
    API_BASE
  },

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + "Sander Photos",
    title: "Sander Photos",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // {
      //   rel: 'stylesheet',
      //   href:
      //     'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      // }
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css' },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@plugins/axios.js',
    '@plugins/$gks.ts'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/vuetify',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: API_BASE
  },

  /**
   * Add middleware
   */
  router: {
    middleware: ['getUser'],
  },

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    iconfont: 'fa',
    
    theme: {
      primary: colors.orange.darken2,
      accent: colors.grey.darken3,
      secondary: colors.amber.darken3,
      info: colors.blue.darken1,
      warning: colors.amber.base,
      error: colors.red.darken2,
      success: colors.green.darken2
    }
  },
  /*
  ** Build configuration
  */
  build: {

    typescript: {
      typeCheck: false
    },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.resolve.alias['@components'] = path.join(__dirname, 'components/');
      config.resolve.alias['@classes'] = path.join(__dirname, '..', 'ts/classes/');
    }
  }
}


/**
 * PWA only in production
 */
if (isProduction) {
  config.modules.push('@nuxtjs/pwa');
  // MANIFEST
  config.manifest = {
    name: "Sander Photos",
    short_name: "Sander",
    lang: 'en',
    display: "standalone",
    background_color: "#ffffff",
    description: "Sander photos",
    theme_color: config.vuetify.theme.primary
  };
}





export default config;