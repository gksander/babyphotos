module.exports = {
  apps: [
    // API
    {
      name: "bp-api",
      cwd: "./backend",
      instances: "max",
      max_memory_restart: '2048M',
      wait_ready: true,
      script: 'npm',
      args: 'start',
      env: {
        NODE_ENV: 'production',
        HOST: "0.0.0.0",
        PORT: 3600
      },
      env_production: {
        NODE_ENV: "production",
        HOST: "0.0.0.0",
        PORT: 3600
      }
    }
  ]
}