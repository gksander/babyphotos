declare namespace $gks {

  /**
   * GKS object injected into nuxt
   */
  export interface $gks {
    data: {
      user: any,
      albums: Array<any>,
      API_BASE: string,
      snackbar: {
        type: string,
        message: string
      },
      loading: {
        value: boolean,
        message: string
      }
    }
    actions: {
      getUser(): void
      logout(): void
      getAlbums(): void
      snackbar(payload: any): void
      setLoading(payload: any): void
    }
  }

  /**
   * Album Data
   */
  export interface IAlbumData {
    id: number;
    title: string;
    description: string;
    photos: Array<IPhotoData>
    photoOrder: Array<number>
  }

  /**
   * Photo data
   */
  export interface IPhotoData {
    id: number
    uuid: string
    originalName: string
    heroWidth: number
    heroHeight: number
    order: number
    description: string
    cover: boolean
  }

  /**
   * User data
   */  
  export interface IUserData {
    id: number
    role: {
      name: string
    }
  }

}